var express = require('express');
var app = express();
var mysql = require('mysql');
var bodyParser = require('body-parser');
var multer  = require('multer');

var profile_image_upload = multer({ storage:multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, 'profile_image/');
		},
		filename: function (req, file, cb) {
			cb(null, new Date().valueOf() + file.originalname);
		}
	})	
});
var post_image_upload = multer({ storage:multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, 'post_image/');
		},
		filename: function (req, file, cb) {
			cb(null, new Date().valueOf() + file.originalname);
		}
	})	
});

var community_image_upload = multer({ storage:multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, 'community_image/');
		},
		filename: function (req, file, cb) {
			cb(null, new Date().valueOf() + file.originalname);
		}
	})	
});

var server = app.listen(3000, function () {
    console.log("Express server has started on port 3000")
})

app.use('/post_image/', express.static('post_image/'));
app.use('/profile_image/', express.static('profile_image/'));
app.use('/community_image/', express.static('community_image/'));
app.use('/test/', express.static('test/'));
app.use(bodyParser.json());

var pool  = mysql.createPool({
	host     : 'localhost',
	port     : 3306, 
	user     : 'root',
	password : require('./token').db_pw,
	database : 'nox'
});

//curl -d '{"MyKey":"My Value"}' -H "Content-Type: application/json" http://127.0.0.1:8081/json/test

app.all('/', function (req, res) {
	res.header("Content-Type", "application/json; charset=utf-8");
    console.log(req.path);
    res.send("Hello World!");
});

//프로파일
app.all('/profile/get/', function(req, res){		
	const uuid = req.body.UUID;
	const sql = "SELECT * FROM PROFILE WHERE UUID = " + mysql.escape(uuid);
	console.log(sql);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			res.header("Content-Type", "application/json; charset=utf-8");
			connection.release();
			res.json(results[0]);
			if (error) {
				res.status(502);
				res.render('error', { error: error });
				throw error;
			}
		});
	});
});

app.post('/profile/*', profile_image_upload.single('file'), function(req, res){
	if(req.path.includes("get"))
		return;
	if(req.path.includes("edit") | req.path.includes("set")){
		console.log(req.file);
		const sql = "INSERT INTO PROFILE(UUID, NAME, INTRO, PROFILE_IMAGE_URL) VALUES(" + mysql.escape(req.body.UUID) + ", " 
						+ mysql.escape(req.body.NAME) + ", " + mysql.escape(req.body.INTRO) + ", " + mysql.escape(req.file.path)	
						+ ") ON DUPLICATE KEY UPDATE NAME=" + mysql.escape(req.body.NAME) + ", INTRO=" + mysql.escape(req.body.INTRO) 
						+ ", PROFILE_IMAGE_URL=" + mysql.escape((req.file != undefined ? req.file.path : ""));
		console.log(sql);
		pool.getConnection(function(err, connection) {
			connection.query(sql, function (error, results, fields) {
				res.header("Content-Type", "application/json; charset=utf-8");
				connection.release();
				res.send("{}");
				if (error) {
					res.status(502);
					throw error;
				}
			});
		});
	}
	else return; 
});


//로그인 및 회원가입
app.all('/register', function(req, res){
	console.log(req.body);
	const id = req.body.ID;
	const pw = req.body.PW;
	// var sha1 = require('node-sha1');
	var uuidv4 = require('uuid/v4');
	const sql = "INSERT INTO LOGIN(UUID, ID, PASSWORD) VALUES("
		+ mysql.escape(uuidv4()) + ', ' + mysql.escape(id) + ', ' + mysql.escape(pw) + ')';
	console.log(sql);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			console.log(results);
			res.send("{}");
			if (error) {
				res.status(502);
				res.render('error', { error: error });
				throw error;
			}
		});
	});
	sha1 = uuidv4 = null;
});

app.all('/login', function(req, res){
	const id = req.body.ID;
	const pw = req.body.PW;
	const sql = "SELECT UUID FROM LOGIN WHERE ID = " + mysql.escape(id) + " AND PASSWORD = " + mysql.escape(pw);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			console.log(results[0]);
			res.header("Content-Type", "application/json; charset=utf-8");
			res.json(results[0]);
			if (error) {
				res.status(502);
				res.render('error', { error: error });
				throw error;
			}

		});
	});
});

//글쓰기 및 수정하기
app.post('/post/write', post_image_upload.single('file'), function(req, res){
	const uuid = require('uuid/v4');
	const sql = "INSERT INTO POST(POST_UUID, AUTHOR_UUID, TITLE, CONTENT, FEELING, IMAGE_URL, IS_SHARE) VALUES(" + mysql.escape(uuid()) + ", " 
				+ mysql.escape(req.body.AUTHOR_UUID) + ", " + mysql.escape(req.body.TITLE) + ", " + mysql.escape(req.body.CONTENT) + ", " 
				+ mysql.escape(req.body.FEELING) + ", " + mysql.escape((req.file != undefined ? req.file.path : "")) 
				+ ", " + mysql.escape(req.body.IS_SHARE) + ")";
	console.log(sql);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			res.header("Content-Type", "application/json; charset=utf-8");
			connection.release();
			console.log(results);
			res.send("{}");
			if (error) {
				res.status(502);
				throw error;
			}
		});
	});
});


app.post('/post/edit', post_image_upload.single('file'), function(req, res){
	const uuid = require('uuid/v4');
	const sql = "UPDATE POST SET TITLE=" + mysql.escape(req.body.TITLE) + ", CONTENT=" + mysql.escape(req.body.CONTENT) + ", FEELING=" 
					+ mysql.escape(req.body.FEELING) + ", IMAGE_URL=" + mysql.escape((req.file != undefined ? req.file.path : ""))  
					+ ", IS_SHARE=" + mysql.escape(req.body.IS_SHARE)
					+ " WHERE POST_UUID=" + mysql.escape(req.body.POST_UUID);
	console.log(sql);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			res.header("Content-Type", "application/json; charset=utf-8");
			connection.release();
			res.send("{}");
			if (error) {
				res.status(502);
				throw error;
			}
		});
	});
});

app.all('/post/get/time', function(req, res){
	res.header("Content-Type", "application/json; charset=utf-8");
	const uuid = req.body.UUID;
	const time = req.body.TIME; //yyyy-mm-dd
	const sql = "SELECT * FROM POST JOIN PROFILE ON PROFILE.UUID = POST.AUTHOR_UUID WHERE AUTHOR_UUID=" + mysql.escape(uuid)
			+ 'AND CONVERT(CHAR(10), POST.WRITE_DATE, 23) = ' + mysql.escape(time) +' ORDER BY POST.WRITE_DATE';
	console.log(sql);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			res.json(results);
			if(error){
				res.status(502);
				throw error;
			}
		}); 
	});
});

app.all('/post/get', function(req, res){
	res.header("Content-Type", "application/json; charset=utf-8");
	const uuid = req.body.UUID;
	const sql = "SELECT * FROM POST JOIN PROFILE ON PROFILE.UUID = POST.AUTHOR_UUID WHERE AUTHOR_UUID=" + mysql.escape(uuid)
			+ ' ORDER BY POST.WRITE_DATE';
	console.log(sql);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			const utf8 = require('utf8');
			var json = JSON.parse("[]".toString('utf8'));
			results.forEach(function(value){
				console.log(value.WRITE_DATE.toISOString().substring(0, 10));
				value["DAY"] = new Date(value.WRITE_DATE.toISOString().substring(0, 10)).getDate();
				value["WEEK_DAY"] = new Date(value.WRITE_DATE.toISOString().substring(0, 10)).getDay();
				json.push(value);
			});
			res.json(json);
			json = null;
			if(error){
				res.status(502);
				throw error;
			}
		}); 
	});
});


app.all('/post/get/ios', function(req, res){
	res.header("Content-Type", "application/json; charset=utf-8");
	const uuid = req.body.UUID;
	const sql = "SELECT * FROM POST JOIN PROFILE ON PROFILE.UUID = POST.AUTHOR_UUID WHERE AUTHOR_UUID="
				+ mysql.escape(uuid) + ' ORDER BY POST.WRITE_DATE';
	console.log(sql);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			console.log(results);
			var jarray = JSON.parse("{}");
			const week = new Array('일', '월', '화', '수', '목', '금', '토');
			results.forEach(function(value){
				console.log(value);
				value["DAY"] = value.WRITE_TIME.getDate();
				value["WEEK_DAY"] = week[value.WRITE_TIME.getDay()];
				if(jarray[value.WRITE_DATE.toISOString().substring(0, 10)] != undefined){
					jarray[value.WRITE_DATE.toISOString().substring(0, 10)].push(value);
				}else{
					jarray[value.WRITE_DATE.toISOString().substring(0, 10)] = JSON.parse("[]");
					jarray[value.WRITE_DATE.toISOString().substring(0, 10)].push(value);
				}
			});
			console.log(jarray);
			res.json(jarray);
			jarray = null;
			if(error){
				res.status(502);
				throw error;
			}
		}); 
	});
});
//TODO : 이거 나도 되는지 잘모름, 테스트 필요
app.all('/community/search', function(req, res){
	res.header("Content-Type", "application/json; charset=utf-8");
	const name = req.body.NAME;
	const sql = `SELECT * FROM COMMUNITY WHERE NAME LIKE '%${name}%'`;
	console.log(sql);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			res.json(results);
			if(error){
				res.status(502);
				throw error;
			}
		}); 
	});
});

app.all('/community/user/list', function(req, res){
	res.header("Content-Type", "application/json; charset=utf-8");
	const uuid = req.body.UUID;
	const sql = "SELECT USER_UUID FROM COMMUNITY_USER WHERE COMMUNITY_UUID=" + mysql.escape(uuid);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			if(error){
				res.status(502);
				throw error;
			}
			res.json(results);
		}); 
	});
});

app.all('/community/get', function(req, res){
	res.header("Content-Type", "application/json; charset=utf-8");
	const uuid = req.body.UUID;
	const sql = "SELECT * FROM COMMUNITY_USER "
				+ "JOIN PROFILE ON PROFILE.UUID = COMMUNITY_USER.USER_UUID "
				+ "JOIN POST ON POST.AUTHOR_UUID = COMMUNITY_USER.USER_UUID "
				+ "WHERE COMMUNITY_USER.COMMUNITY_UUID=" + mysql.escape(uuid)
				+ " AND POST.IS_SHARE=1";
	console.log(uuid);
	console.log(sql);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			if(error){
				res.status(502);
				throw error;
			}
			res.json(results);
		}); 
	});
});

app.all('/community/user/add', function(req, res){
	res.header("Content-Type", "application/json; charset=utf-8");
	const community_uuid = req.body.COMMUNITY_UUID;
	const user_uuid = req.body.USER_UUID;
	const sql = "INSERT INTO COMMUNITY_USER(USER_UUID, COMMUNITY_UUID) VALUES(" 
				+ mysql.escape(user_uuid) + ", " + mysql.escape(community_uuid) + ")";
	console.log(sql);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			if(error){
				res.status(502);
				throw error;
			}
			res.send("{}");
		}); 
	});
});

app.post('/community/add', community_image_upload.single('file'), function(req, res){
	const uuid = require("uuid/v4");
	const name = req.body.NAME;
	const path = (req.file != undefined ? req.file.path : "");
	const sql = "INSERT INTO COMMUNITY(UUID, NAME, BACKGROUND_URL) VALUES(" + mysql.escape(uuid()) + ', ' 
					+ mysql.escape(name) + ', ' + mysql.escape(path) + ') ON DUPLICATE KEY UPDATE NAME='
					+ mysql.escape(name) + ', BACKGROUND_URL=' + mysql.escape(path);
	console.log(sql);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			res.send("{}");
			if(error){
				res.status(502);
				throw error;
			}

		}); 
	});
});

app.post('/community/edit', community_image_upload.single('file'), function(req, res){
	const name = req.body.NAME;
	const path = (req.file != undefined ? req.file.path : "");
	const uuid = req.body.UUID;
	const sql = "INSERT INTO COMMUNITY(UUID, NAME, BACKGROUND_URL) VALUES(" + mysql.escape(uuid) + ', ' 
					+ mysql.escape(name) + ', ' + mysql.escape(path) + ') ON DUPLICATE KEY UPDATE NAME='
					+ mysql.escape(name) + ', BACKGROUND_URL=' + mysql.escape(path);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			res.send("{}");
			if(error){
				res.status(502);
				throw error;
			}

		}); 
	});
});


app.all('/comment/get', function(req, res){
	res.header("Content-Type", "application/json; charset=utf-8");
	const uuid = req.body.UUID;
	const sql = "SELECT * FROM POST_COMMENT JOIN PROFILE ON PROFILE.UUID = "
				+ "POST_COMMENT.AUTHOR_UUID WHERE POST_UUID=" + mysql.escape(uuid)
				+ " ORDER BY POST_COMMENT.WRITE_DATE";
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			if(error){
				res.status(502);
				throw error;
			}
			res.json(results);
		}); 
	});
});

app.all('/comment/write', function(req, res){
	res.header("Content-Type", "application/json; charset=utf-8");
	const uuid = require('uuid/v4');
	const comment_uuid = uuid();
	const post_uuid = req.body.POST_UUID;
	const author_uuid = req.body.AUTHOR_UUID;
	const content = req.body.CONTENT;
	const sql = "INSERT INTO POST_COMMENT(COMMENT_UUID, POST_UUID, AUTHOR_UUID, CONTENT) VALUES("
					+ mysql.escape(comment_uuid) + ', ' + mysql.escape(post_uuid) + ', '
					+ mysql.escape(author_uuid) + ', ' + mysql.escape(content) + ')';
	console.log(sql);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			if(error){
				res.status(502);
				throw error;
			}
			res.send("{}");
		}); 
	});
});

app.all('/comment/delete', function(req, res){
	res.header("Content-Type", "application/json; charset=utf-8");
	const uuid = req.body.UUID;
	const sql = "DELETE FROM POST_COMMENT WHERE COMMENT_UUID=" + mysql.escape(uuid);
	console.log(uuid);
	console.log(sql);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			if(error){
				res.status(502);
				throw error;
			}
			res.json("{}");
		}); 
	});
});

app.all('/comment/edit', function(req, res){
	res.header("Content-Type", "application/json; charset=utf-8");
	const uuid = req.body.COMMENT_UUID;
	const content = req.body.CONTENT;
	const sql = "UPDATE POST_COMMENT SET CONTENT=" + mysql.escape(content) + " WHERE COMMENT_UUID=" + mysql.escape(uuid);
	console.log(uuid);
	console.log(sql);
	pool.getConnection(function(err, connection) {
		connection.query(sql, function (error, results, fields) {
			connection.release();
			if(error){
				res.status(502);
				throw error;
			}
			res.json("{}");
		}); 
	});
});
// app.all('/json/*', function (req, res){
	// res.header("Content-Type", "application/json; charset=utf-8");
	// console.log(req.body);
	// pool.getConnection(function(err, connection) {
		// connection.query('SELECT * from TEST', function (error, results, fields) {
			// connection.release();
			// res.json(results);
			// if(error){
				// res.status(502);
				// throw error;
			// }
		// });
	// });
// });
