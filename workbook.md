## HOW TO USE SERVER??

* 모든 API는 함수 명 앞에 http://hongmu.wiki:3000/ <- 이 주소 붙여서 사용하면 됨
* MULTI_PART로 인코딩해서 보내야하는 API들은 해당 API함수 명 옆에 --MULTI_PART라고 적혀있음

### profile 관련 API
#### /profile/get
사용자의 PROFILE을 가져오는 API

님들이 뭘 하실지는 모르지만 UUID 값으로 그 사람의 프로파일 정보를 가져오기 위해서
해당 API에 아래 처럼 사용자의 UUID값을 넣은 JSON을 보내서 가져올수 있음
```json
{
	"UUID":"~!!~"
}
```
그러면 우리의 착한 서버찡은 거부하지 않고 이렇게 반환을 해줄거임
```json
{
	"UUID":"UUID",
	"NAME":"GG",
	"INTRO":"5R216T37Y4U85",
	"IMAGE_URL":"QRTGEYURHJIT",
}
```

####/profile/edit, /profile/set (URL만 다름, 파라미터는 같음) --MULTI_PART
PROFILE을 수정하거나 설정할 때 사용하는 API

프로파일을 수정하거나 설정을 할 때에는 위의 주소로 아래의 형식으로 MULTI_PART를 
보내주면 됨
```
	"UUID":"1234",
	"NAME":"GG",
	"INTRO":"5R216T37Y4U85",
	"file":"이미지"
  ```

만약 이걸 처리하는 과정에서 오류가 생기면 HTTP 502에러를 던져주고, 만약 성공하면 200
```http_code
IF ERR
(CODE 502)

ELSE
(CODE 200)
```

### POST관련 API
#### /post/get
해당 사용자의 게시글을 가져오는 API

post를 가져오기 위해서 해당 사용자의 UUID값을 JSON으로 서버측으로 날려줘야함
```json
{
	"UUID":"123edsa"
}
```
그러면 서버에서는 해당 UUID의 사용자가 작성한 POST들을 JSON ARRAY로 반환해줌
```json
[
	 {
      "POST_NUM":"해당 개시글의 고유 번호",
      "POST_UUID":"해당 개시글의 UUID",
      "AUTHOR_UUID":"작성자의 UUID",
      "TITLE" : "제목",
      "WRITE_DATE" : "작성한 날짜와 시간",
      "CONTENT":"개시글 내용",
      "FEELING":"현재 감정(?)",
      "IMAGE_URL":"게시글 이미지"
   },
]
```

###/post/write --MULTI_PART
게시글을 올리는 API

```
"AUTHOR_UUID" : "ASDFDFSA",
"TITLE" : "TITLE",
"CONTENT" : "CONTENT",
"FEELING" : "HAPPY",
"file" : 이미지,
"IS_SHARE":"공유할건지 1또는 0, 1이면 공유"
```

### /post/edit --MULTI_PART
올린 게시글을 수정할 때 API

```
"POST_UUID":"ASDFDASF"
"TITLE":"ASFDIHUJ;SOAKP",
"CONTENT":"ASFTDUGHYJIOK",
"FEELING":"R162TY3GU4EHR5I",
"file":이미지,
"IS_SHARE":"공유할건지 1또는 0, 1이면 공유"
```

위의 두 API모두 실패하면 502, 성공하면 200 HTTP CODE가 반환됨

### 로그인 및 회원가입 관련 API
####/login
로그인 하는 API

ID와 PW를 보내주면 유저의 UUID값을 반환해준다.

PW는 SHA-1로 암호화

INPUT
```json
{
	"ID":"qtwrujighy",
	"PW":"~sdahifljkasdf"
}
```
OUTPUT
```json
{
	"UUID":"451	26twq3e7yuritg"
}
```

####/register
회원가입 API

ID와 PW를 보내주면 회원 등록을 해준다.
```json
{
	"ID":"`1234",
	"PW":"`12345"
}
```
### 커뮤니티 관련 API
#### /community/search
커뮤니티를 검색하는 API

커뮤니티 이름의 일부를 넣어서 JSON으로 보내주면 
이름에 해당 문자열이 포함되는 커뮤니티들을 반환해줌

INPUT
```json
{
	"NAME":"걍 암거나"
}
```

OUTPUT
```json
[
  {
  	"UUID":"ASDASD",
  	"NAME":"3000원",
  	"IMAGE_URL":"sadfsdf"
  }
]
```

#### /community/user/list
커뮤니티 내의 사용자들을 조회하는 API

커뮤니티의 UUID값을 JSON으로 보내주면 커뮤니티 내의 사용자들의 UUID값을 반환해준다.

INPUT
```json
{
	"UUID":"커뮤니티 UUID"
}
```

OUTPUT 
```json
[
	{"USER_UUID":"asdf"},
	{"USER_UUID":"asdf"},
	{"USER_UUID":"asdf"},
	{"USER_UUID":"asdf"},
]
```

#### /community/get
커뮤니티 내의 포스트를 가져오는 API

커뮤니티의 UUID값을 JSON으로 보내주면 해당 커뮤니티의 게시글들을 반환해준다

INPUT
```json
{
	"UUID":"커뮤니티 UUID"
}
```
OUTPUT
```json
[
	 {
      "POST_NUM":"해당 개시글의 고유 번호",
      "POST_UUID":"해당 개시글의 UUID",
      "AUTHOR_UUID":"작성자의 UUID",
      "TITLE" : "제목",
      "WRITE_DATE" : "작성한 날짜와 시간",
      "CONTENT":"개시글 내용",
      "FEELING":"현재 감정(?)",
      "IMAGE_URL":"게시글 이미지"
   }
]
```

#### /community/get/time
커뮤니티 내의 포스트를 가져오는 API, 단 보내준 날짜에 작성된 글만 반환해준다.

커뮤니티의 UUID값과 원하는 날짜를 JSON으로 보내주면 해당 커뮤니티의 게시글들을 반환해준다

INPUT
```json
{
	"UUID":"커뮤니티 UUID"
  "TIME":"yyyy-mm-dd"
}
```
OUTPUT
```json
[
	 {
      "POST_NUM":"해당 개시글의 고유 번호",
      "POST_UUID":"해당 개시글의 UUID",
      "AUTHOR_UUID":"작성자의 UUID",
      "TITLE" : "제목",
      "WRITE_DATE" : "작성한 날짜와 시간",
      "CONTENT":"개시글 내용",
      "FEELING":"현재 감정(?)",
      "IMAGE_URL":"게시글 이미지"
   }
]
```
####/community/user/add
커뮤니티에 유저를 추가하는 API

커뮤니티의 UUID값과 추가할 유저의 UUID값을 보내면 커뮤니티에 추가된다.

INPUT
```json
{
	"COMMUNITY_UUID":"커뮤니티 UUID",
	"USER_UUID":"유저 UUID"
}
```

#### /community/add --MULTI_PART
커뮤니티를 생성하는 API

INPUT
```
"NAME":"갓갓 3명과 잉여 경달 하나",
"file":이미지
```

#### /community/edit --MULTI_PART
만들어진 커뮤니티를 수정하는 API

INPUT
```
"UUID":"커뮤니티 UUID",
"NAME":"갓갓 3명과 잉여 경달 하나",
"file":이미지
```

### 댓글 관련 API
#### /comment/get
post(일기들)에 작성된 댓글을 받아오는 API

일기의 POST_UUID를 JSON 으로 보내주면 댓글 리스트를 반환해준다.

INPUT
```json
{
  "UUID":"POST(일기) UUID"
}
```

OUTPUT
```json
[
  {
    "COMMENT_UUID":"댓글 UUID",
    "POST_UUID":"글의 UUID",
    "AUTHOR_UUID":"작성자의 UUID",
    "CONTENT":"댓글 내용",
    "WRITE_DATE":"작성 시기"
  }
]
```

#### /comment/write
일기에 댓글을 작성한다.

작성자의 UUID, 작성 글의 UUID, 댓글 내용을 보내주면 댓글을 업로드 해준다.

INPUT
```json
{
  "POST_UUID":"글의 UUID",
  "AUTHOR_UUID":"댓글 작성자의 UUID",
  "CONTENT":"댓글 내용"
}
```

#### /comment/delete
작성된 댓글을 삭제 한다.

댓글의 UUID를 보내면 해당 댓글을 삭제해준다.

INPUT
```json
{
  "UUID":"댓글 UUID"
}
```

#### /comment/edit
작성된 댓글을 수정한다.

댓글의 UUID와 수정할 내용을 JSON으로 보내주면 해당 댓글을 수정해준다.

INPUT
```json
{
  "COMMENT_UUID":"댓글 UUID",
  "CONTENT":"댓글 수정본"
}
```

